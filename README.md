# **Scaling**

If you are running a website, web service, or application, its success relies on the amount of network traffic it receives. It is common to underestimate just how much traffic your system will incur, especially in the early stages. This could result in a crashed server and/or a decline in your service quality. Scalability describes a system’s elasticity. While we often use it to refer to a system’s ability to grow, it is not exclusive to this definition. We can scale down, scale up, and scale out accordingly.

## **Solutions**

Solutions to this problem include, the usage of load balancer and execute vertical and horizontal scaling. Let us understand these solutions in detail.

## **Load balancer**

A load balancer acts as the “traffic cop” sitting in front of your servers and routing client requests across all servers capable of fulfilling those requests in a manner that maximizes speed and capacity utilization and ensures that no one server is overworked, which could degrade performance. If a single server goes down, the load balancer redirects traffic to the remaining online servers. When a new server is added to the server group, the load balancer automatically starts to send requests to it.

In this manner, a load balancer performs the following functions:

1. Distributes client requests or network load efficiently across multiple servers
2. Ensures high availability and reliability by sending requests only to online servers
3. Provides the flexibility to add or subtract servers as demand dictates

### **Load Balancing Algorithms**

Some most commonly used load-balancing algorithms are:
* **Round Robin** - Requests are distributed across the group of servers sequentially.
* **Least Connections** -  A new request is sent to the server with the fewest current connections to clients.
* **Random** - Sends requests to the server selected by a formula that combines the
fastest response time and fewest active connections.

## Vertical and Horizontal Scaling

Horizontal Scaling (aka scaling out) refers to adding additional nodes or machines to your infrastructure to cope with new demands. If you are hosting an application on a server and find that it no longer has the capacity or capabilities to handle traffic, adding a server may be your solution. Whereas Vertical Scaling (aka scaling up) describes adding additional resources to a system so that it meets demand.

### Advantages

#### Horizontal Scaling
* Scaling is easier from a hardware perspective as the availability of tools in the market is huge.
* Long-time solution can be done.
* Performance is increased and reliance is more.

#### Vertical Scaling
* Cost effective. This only advantage is enough to make a project efficient.
* Less complex communication structure as no new level is being created.
* The need to change the software is removed.

### Disadvantages 

#### Horizontal Scaling
* Increased initial cost.
* Complexity of the system is increased due to the introduction of new levels.

#### Vertical Scaling
* Unless you have a backup server that can handle operations and requests, you will need some considerable downtime to upgrade your machine. 
* Having all your operations on a single server increases the risk of losing all your data if a hardware or software failure was to occur. 


## References

* Horizontal vs Vertical Scaling [Codewithharry](https://www.youtube.com/watch?v=3GGPsX-s5-M) 
* What is a load balancer [IBM Technology](https://www.youtube.com/watch?v=sCR3SAVdyCc)
* What is load balancing [Nginx](https://www.nginx.com/resources/glossary/load-balancing/)
* Horizontal Vs. Vertical Scaling: How Do They Compare? [CLOUDZERO](https://www.cloudzero.com/blog/horizontal-vs-vertical-scaling)

