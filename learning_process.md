# Learning Process

## 1. How to Learn Faster with the Feynman Technique

### Question 1: What is the Feynman Technique? Paraphrase the video in your own words.
The Feynman technique suggests how one can understand a topic better and quickly. Feynman's technique suggests four steps to understand a topic in deep. They are:

* Take a piece of paper and write the concept's name at the top.
* Explain the topic using simple language (Not simple English simple language).
* Identify problem areas, then go back to the sources to review.
* Pin-point any complicated terms and challenge yourself to simplify them.


### Question 2: What are the different ways to implement this technique in your learning process?
To implement this technique in our learning process, we can explain programming fundamentals to our colleagues or college juniors we are in contact with. Another idea suggested by the Youtuber was to explain the topic to a child with simple examples such as candies, cartoons, or super-heroes.

## 2. Learning How to Learn TED talk by Barbara Oakley

### Question 3 : Paraphrase the video in detail in your own words.

The best thing one can learn is how to learn. The brain works in two modes. One is active and focused and the other one is relaxed. Historically its been seen that inventors like Thomas Eddison used relaxed techniques to solve complex problems. Sitting on a chair holding a ball until one falls asleep, and then again starting working, has been one of the most promising way to understand a complex phenomenon or solve a math problem.

### Question 4 : What are some of the steps that you can take to improve your learning process?

By following the steps suggested in the Pomodoro technique, one can improve their learning. The Pomodoro technique suggests working for 25 minutes straight and then doing some fun activities. This way one can improve his learning as well as his ability to break as well.

## 3. Learn Anything in 20 hours

### Question 5 : Your key takeaways from the video? Paraphrase your understanding.

The amount of time it takes to learn a new skill is not 10,000 hours as many research suggests, but requires only 20 hours of dedicated practice.

Some key points:
* The time to learn a new skill depends upon to what degree we want to ace the skill.
* The major barrier to skill acquisition is not intellectual, it's emotional. 

### Question 6 : What are some of the steps that you can while approaching a new topic?

1. Deconstruct the skill
2. Learn enough to self-correct
3. Remove the practice barrier
4. Practice at least 20 hours




