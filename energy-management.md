# Energy Management

## Manage energy not time.

### Question 1: What are the activities you do that make you relax - Calm quadrant?

Some activities that make me feel relax are -

- Playing football with my friends.
- Watching football.
- Listening to music.
- Watching movies.
- Watching Dr Vikas's video.

### Question 2 : When do you find getting into the Stress quadrant?

- When I have a lot of work pending.
- When I get stuck in solving problems.
- When I find myself not understanding concepts.
- When I have less time to complete tasks before deadlines.

### Question 3: How do you understand if you are in the Excitement quadrant?

- When national anthem plays at in cricket matches.
- When someone calls for a footbal match.
- When someone tells some good news.

## Sleep is your superpower

### Question 4 : Paraphrase the Sleep is your Superpower video in detail.

Sleep is a very important activity in life. It is essential to sleep enough to stay in good health and spirits. Studies have shown that people with less sleep are more prone to have health issues like increased chances of heart attacks, poor immunity, and early onset of aging.

### Question 5 : What are some ideas that you can implement to sleep better?

Relaxing the body, fixing a particular time to sleep, an optimal room temperature, frequent exercise, and having a fixed time to have dinner can help with better sleep.

## Brain Changing Benefits of Exercise

###Question 6 : Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.

There are several brain-changing benefits of exercise. Some of them are-

- Maintaining focus for longer stretches of time than earlier.
- Slower onset of ageing, and better chances of avoiding incurable diseases.
- Helps reduce obesity and makes you feel good due to releasae of dopamine.
- Helps in tackling stress.

### Question 7 : What are some steps you can take to exercise more?

Building a habit is more important than just having motivation, because with habit the brain automatically ensures the regularity of exercise, hence making us mentally and physically stronger. It only takes some initial effort and willingness to do so. One can easily add some physical activities like running an extra mile, freestyle jumping, hand movements, and some aerobics to exercise the heart and build up stamina.
