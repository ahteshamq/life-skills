# Listening and Active Communication

## 1. Active Listening


### Question 1

Active listening is the act of carefully listening and examining what a person is saying.

Steps for Active Listening:

1. Pay attention to a speaker without getting distracted.
2. Wait for the person to finish what they are saying, then raise queries.
3. Listen with proper body language.
4. Take notes in impoertant conversations if possible.
5. Ask for additional information if required.
6. Make proper eye contact with the speaker.

### Question 2 According to Fisher's model, what are the key points of Reflective Listening? (Write in your own words, use simple English)

* Avoid distractions.
* Communicate with speaker frequently.
* Conclude what they said in brief.
* Reflect the intention of speaker with words or expressions.

### Question 3 What are the obstacles in your listening process?

* Thinking about something else while speaker is speaking.
* Not looking at speaker.
* Unable to process thoughts.

### Question 4 What can you do to improve your listening?

* By making notes of what they are saying.
* Summarizing in our own words what they just said.
* Understanding the topic better.

### Question 5 When do you switch to Passive communication style in your day to day life?

When we are talking with seniors, elders and members of management in office.

### Question 6 When do you switch into Aggressive communication styles in your day to day life?

During any fight or when someone is not responding properly during any conversation.

### Question 7 When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

When I hang out with friends and family.

### Question 8 How can you make your communication assertive? You can analyse the videos and then think what steps you can apply in your own life? (Watch the videos first before answering this question.)

* By Choosing the correct timing and the correct way to speak things.
* By applying logical thinking on every situation and not getting emotional.
