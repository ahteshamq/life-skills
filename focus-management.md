# Focus Management

## What is Deep Work

### Question 1 : What is Deep Work?

Continuously working without getting distracted by any external forces such as social media, emails etc.

## Optimal duration for deep work

### Question 2 : Paraphrase all the ideas in the above videos and this one in detail.

- The first video describes deep work, and the duration of work without taking breaks, according to Cal, the minimum duration of deep work should be 1 hour.
- In the second video, Cal talks about deadlines and their effectiveness and why they work. Reasons he gave:
    - They serve as motivation to work.
    - Deadlines help regulate breaks as one can plan how long they need to work before taking a break.
- Third video talks about The 'Deep Work' book by Cal Newport. In this book Cal has defined what deep work is, how can we plan it. Some methods include:
    - Scheduling break and to have one social media break in the day, not all breaks should have media interaction.
    - Scheduling Deep and Shallow work period in a day.

### Question 3 : How can you implement the principles in your day to day life?

Some steps that implement deep work in our day-to-day life are :-

- At least practice deep work for 1 hour a day.
- Schedule breaks, and schedule social media break.
- Continue to follow deep-Shallow work cycle every day.
- Get adequate sleep.

## Dangers of Social Media

### Question 4 : Your key takeaways from the video.

In this TEDx video Dr. Cal talks about :

- Social media is not core of success in this time.
- Social Media wastes too much time and is unhealthy for mind and body.
- Social Media keeps distracting as it is designed to keep your attention for as long as possible.
- Use of social media is bad for one's mental health as it leads to comparison of lifestyles which can depress a person.
