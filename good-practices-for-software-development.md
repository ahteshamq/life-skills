# Question 1
### What is your one major takeaway from each one of the 6 sections So 6 points in total.

## Answer 1
- Always make notes during meetings and ask for feedback during implementations.

- Communicate effectively with teammates.

- Clear your doubts, explain things using pictorial data, and look at how issues are reported in large projects.

- Get to know your teammates by making time for them and finding out their work schedules.

- Be aware and mindful of other team members' schedules and communication preferences.

- Practice deep work, manage your food, and social life, exercise, and use time tracking and productivity tools to stay focused and energized.

# Question 2
### Which area do you think you need to improve on? What are your ideas to make progress in that area?

## Answer 2
- Have an eye on the industry, and keep learning from seniors.
- Need to contribute to open source and practice regularly and consistently to get familiar with best practices and industry standards.
- Always ask for feedback, and handle criticism.
