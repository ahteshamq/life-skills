# Grit and Growth Mindset

## 1. Grit

### Q1. Paraphrase (summarize) the video in a few lines. Use your own words.

- People with strong perseverance achieve more than those who are just talented.
- Completing our tasks on a deadline(self-defined) can help us develop grit.
- Irrespective of the environment gritty people get their success.
- An open and growing attitude can help us develop grit within ourselves.

### Q2. What are your key takeaways from the video to take action on?

- One should not get demotivated if they get fail, instead they should keep learning and try.
- IQ is not the reason for success, so your IQ does not guarantee success or failure.
- Grit is the desire to achieve success in any environment be it good, bad, or worse.
- Grit is what defines one's success.
- Grown mentality develops grit.

## 2. Introduction to Growth Mindset

### Q3. Paraphrase (summarize) the video in a few lines in your own words.

- Our perspective of life describes our personality.
- To make a change we need to change our mindset.
- The first one should make an effort to learn, try on challenges, use them to learn, and always see feedback as a positive aspect of our life.

### Q4. What are your key takeaways from the video to take action on?

- Make a mindset that skills can be learned and are not developed at born.
- Never run from challenges.
- Always look for challenges as a part of learning, and try them.
- Always take feedback seriously and take action on it.

## 3. Understanding Internal Locus of Control

### Q5. What is the Internal Locus of Control? What is the key point in the video?

- It is the state of mind where someone is always motivated and does not see other external factors as an obstacle to their success in a task.
- This video focuses on how to stay motivated in any condition and stop complaining about things that are not in our hands.

### Q6. Paraphrase (summarize) the video in a few lines in your own words.

- Do not think that you cannot increase your abilities.
- Do not let your current abilities stop you from trying new things.
- Always believe in yourself.
- Create a path to your goals, by making new routines
- Do not let failure stop you from achieving your goals.

### Q7. What are your key takeaways from the video to take action on?

- Always focus on your work, instead of thinking about your current capabilities.
- Mistakes are part of the journey, do not get discouraged by them.
- Face all obstacles irrespective of their hardness.

### Q8. What are one or more points that you want to take action on from the manual? (Maximum 3)

- I will stay with a problem till I complete it. I will not quit the problem.
- I will understand each concept properly.
- I will always be enthusiastic. I will have a smile on my face when I face new challenges or meet people.



